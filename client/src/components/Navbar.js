import React from 'react'
import { NavLink } from 'react-router-dom'

const Navbar = props => {
   return (
   <div className='navbar'>
    {
      props.isLoggedIn
      ? [
          <button onClick={()=>props.logout()}>logout</button>,
          <NavLink
            exact
            style={styles.default}
            activeStyle={styles.active}
            to={"/users"}
          >
            Users
          </NavLink>
        ]
      : [
          <NavLink
                exact
                style={styles.default}
                activeStyle={styles.active}
                to={"/register"}
            >
            Register
          </NavLink>,
          <NavLink
            exact
            style={styles.default}
            activeStyle={styles.active}
            to={"/"}
          >
            Login
          </NavLink>
        ]
    }
   </div>
  )
}
   
export default Navbar

const styles = {
  active: {
    color: "gray"
  },
  default: {
    textDecoration: "none",
    color: "white"
  }
};
