import React, { useState, useEffect } from "react";
import { socket } from "../socket";

const Users = props => {
  //========================================================
  const [users, setUsers] = useState([]);
  const [message, setMessage] = useState({
    text: "",
    display: "none"
  });
  //========================================================
  const userId = JSON.parse(localStorage.getItem("userId"));
  const user2 = JSON.parse(localStorage.getItem("user2"));
  const token = JSON.parse(localStorage.getItem("token"));
  //========================================================
  useEffect(() => {
    //debugger
    //getConnectedUsers()
    socket.emit("get_connected_users", token);
    socket.on("found_users", connectedUsers => {
      setUsers([...connectedUsers]);
    });
    socket.on("room_create_message", (user2, message) => {
      localStorage.setItem("user2", JSON.stringify(user2));
      //debugger
      setMessage({ ...message, text: `${message}`, display: "flex" });
    });
  }, []);
  //========================================================
  //===========  CREATE ROOM INTENT  =======================
  //========================================================
  const handleCreateRoom = oponentId => {
    localStorage.setItem("user2", JSON.stringify(oponentId));
    socket.emit("create_room_intent", token, oponentId);
    //=======  receive response from the other user =======
    socket.on("create_room_intent_response", value => {
      value
        ? props.history.push(`/room`)
        : alert("Sorry, the other user refuse your invitation");
    });
  };
  //========================================================
  const handleCreateRoomResponse = value => {
    return value
      ? (socket.emit("response_to_create_room", token, true, user2),
        props.history.push(`/room`))
      : (socket.emit("response_to_create_room", token, false, user2),
        setMessage({ ...message, text: ``, display: "none" }));
  };
  return (
    <div className="users_container">
      {users.map((user, idx) => {
        return (
          <div key={idx} className="single_user">
            <div>{user.email}</div>
            <button onClick={() => handleCreateRoom(user._id)}>play</button>
          </div>
        );
      })}
      <div style={{ display: message.display }} className="message_container">
        {message.text}
        <button onClick={() => handleCreateRoomResponse(true)}>YES</button>
        <button onClick={() => handleCreateRoomResponse(false)}>NO</button>
      </div>
    </div>
  );
};

export default Users;
