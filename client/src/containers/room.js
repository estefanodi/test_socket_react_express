import React, { useState } from "react";
import { socket } from "../socket";

const Room = () => {
  const user2 = JSON.parse(localStorage.getItem("user2"));
  const [points, setPoints] = useState({
    a0: 0,
    b0: 0,
    a1: 0,
    b1: 0,
    a2: 0,
    b2: 0
  });
  const token = JSON.parse(localStorage.getItem("token"));
  const [values] = useState([
    { value: Math.floor(Math.random() * 100), selected: false },
    { value: Math.floor(Math.random() * 100), selected: false },
    { value: Math.floor(Math.random() * 100), selected: false }
  ]);

  const get_other_user_points = () => {
    const temp = points;
    socket.on("other_user_move", result => {
      temp["b" + result.i] = result.value;
      setPoints({ ...temp, ["b" + result.i]: result.value });
    });
  };
  get_other_user_points();

  const play = (i, value) => {
    const temp = points;
    socket.emit("play", token, i, value, user2);
    temp["a" + i] = value;
    setPoints({ ...temp, ["a" + i]: value });
  };
  return (
    <div className="room_container">
      <div className="cards_container">
        {values.map((item, idx) => {
          return <div key={idx} className="card opponent_cards"></div>;
        })}
      </div>
      <div className="middle">
        {values.map((item, idx) => {
          return (
            <div key={idx} className="points">
              <div className="small_points">{points["a" + idx]}</div>
              <div className="small_points">{points["b" + idx]}</div>
            </div>
          );
        })}
      </div>
      <div className="cards_container">
        {values.map((item, idx) => {
          return (
            <div
              key={idx}
              onClick={() => play(idx, item.value)}
              className="card"
            >
              {item.value}
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default Room;
