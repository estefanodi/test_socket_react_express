const Users = require("../models/users.models");
const jwt = require("jsonwebtoken");
const jwt_secret = process.env.JWT_SECRET;
const connectedUsers = [];
//========================================================================
//========================================================================
//========================================================================
const verify_token = async token => {
  const decoded = await jwt.verify(token, jwt_secret);
  return decoded;
};
//========================================================================
//========================================================================
//========================================================================
const getConnectedUsers = async (_, socket, token) => {
  try {
    //==========  check if the token is valid
    const decoded = await verify_token(token);
    //==========  check if I'm already in the connectedUsers array
    const index = connectedUsers.findIndex(user => user.userId === decoded._id);
    index === -1
      ? connectedUsers.push({ userId: decoded._id, socketId: socket.id })
      : (connectedUsers[index].socketId = socket.id);
    //========== get connected users ids
    const ids = [];
    connectedUsers.forEach(user => {
      user.userId !== decoded._id ? ids.push(user.userId) : null;
    });
    //========== get connected users from the database
    const users = await Users.find({ _id: { $in: ids } });
    //========== send all connected users to the client
    socket.emit("found_users", users);
  } catch (error) {
    console.log("error ===>", error);
  }
};
//========================================================================
//========================================================================
//========================================================================
const createRoomIntent = async (io, _, token, user2) => {
  try {
    const decoded = await verify_token(token);
    // do something here if the token is not valid
    const user2idx = connectedUsers.findIndex(user => user.userId === user2);
    io.to(`${connectedUsers[user2idx].socketId}`).emit(
      "room_create_message",
      decoded._id,
      `${decoded.email} want to play with you`
    );
  } catch (error) {
    console.log("error ===>", error);
  }
};
//========================================================================
//========================================================================
//========================================================================
const responseToCreateRoom = async (io, _, token, value, user1) => {
  try {
    const decoded = await verify_token(token);
    // do something here if the token is not valid
    const user1idx = connectedUsers.findIndex(user => user.userId === user1);
    io.to(`${connectedUsers[user1idx].socketId}`).emit(
      "create_room_intent_response",
      value
    );
  } catch (error) {
    console.log("error ===>", error);
  }
};
//========================================================================
//========================================================================
//========================================================================
const play = async (io, _, token, i, value, user2) => {
  try {
    const decoded = await verify_token(token);
    // do something here if the token is not valid
    const index = connectedUsers.findIndex(user => user.userId === user2);
    io.to(`${connectedUsers[index].socketId}`).emit("other_user_move", {
      i,
      value
    });
  } catch (error) {
    console.log("error ===>", error);
  }
};

module.exports = {
  getConnectedUsers,
  createRoomIntent,
  responseToCreateRoom,
  play
};
